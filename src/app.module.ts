import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as joi from 'joi';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductsModule } from './products/products.module';
import { enviroment } from './enviroments';
import config from './config';


@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: enviroment[process.env.NODE_ENV || '.env'],
      load: [config],
      isGlobal: true,
      validationSchema: joi.object({
        API_KEY: joi.string().required(),
        DATABASE_USER: joi.string().required(),
        PORT: joi.number().required(),
      })
    }),
    ProductsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
