import { Inject, Injectable } from '@nestjs/common';

import { ConfigType } from '@nestjs/config';
import config from './config';

@Injectable()
export class AppService {
  constructor(
    @Inject(config.KEY) private configService: ConfigType<typeof config>,
  ) {}

  getHello(): string {
    const apiKey = this.configService.app.apiKey;
    const dbUser = this.configService.database.user;
    return `La apikey es ${apiKey} y el usuario de la base de datos es ${dbUser} `;
  }
}
