import { registerAs } from '@nestjs/config';

export default registerAs('config', () => {
  return {
    database: {
      user: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
    },
    app: {
      apiKey: process.env.API_KEY,
      port: process.env.PORT,
    },
  };
});
