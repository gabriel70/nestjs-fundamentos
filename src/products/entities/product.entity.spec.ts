import { Product } from './product.entity';

describe('Products Entity', () => {
  it('should be defined', () => {
    expect(new Product()).toBeDefined();
  });
});
