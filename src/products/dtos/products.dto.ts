import { IsNotEmpty, IsNumber, IsString, IsUrl } from 'class-validator';
import { PartialType, ApiProperty } from '@nestjs/swagger';

export class CreateProductsDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly description: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  readonly price: number;

  @IsUrl()
  @IsNotEmpty()
  @ApiProperty()
  readonly url: string;
}

export class UpdateProductDto extends PartialType(CreateProductsDto) {}
