import { Injectable, NotFoundException } from '@nestjs/common';

import { Product } from './../entities/product.entity';
import { CreateProductsDto, UpdateProductDto } from './../dtos/products.dto';

@Injectable()
export class ProductsService {
  private counterId = 1;
  private products: Product[] = [
    {
      id: 1,
      name: 'Producto 1',
      description: 'lorem lorem',
      price: 10000,
      url: 'https://i.imgur.com/U4iGx1j.jpeg',
    },
  ];

  findAll() {
    return this.products;
  }

  findOne(id) {
    const product = this.products.find((item) => item.id === id);

    if (!product) {
      throw new NotFoundException(`The product id: ${id} not found`);
    }
    return product;
  }
  create(data: CreateProductsDto) {
    this.counterId = this.counterId + 1;
    const newProduct = {
      id: this.counterId,
      ...data,
    };
    this.products.push(newProduct);
    return newProduct;
  }

  update(id: number, changes: UpdateProductDto) {
    const product = this.findOne(id);
    const index = this.products.findIndex((item) => item.id === id);

    this.products[index] = {
      ...product,
      ...changes,
    };

    return this.products[index];
  }

  remove(id) {
    const index = this.products.findIndex((item) => item.id === id);
    if (index === -1) {
      throw new NotFoundException(`The product id: ${id} not found`);
    }

    this.products.splice(index, 1);

    return true;
  }
}
